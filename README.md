# Simple Music Text (SMT)

Simple Music Text (SMT) is a plain text format for representing music notation. It is designed to be both human-readable and machine-parsable, making it easy to work with both manually and programmatically.

## Overview

SMT files consist of a series of commands that define the music, with each command represented by a keyword followed by one or more arguments. SMT supports nested structures, allowing for the representation of complex musical structures. SMT is also designed to be easily parsable by many programming languages, making it an attractive format for use in music software and web applications.

## Versions

The current version of SMT is version 2.2. Previous versions of SMT are also available, but they may lack some of the features and improvements of the latest version.

## Syntax

The syntax of SMT is designed to be simple and easy to read. Here is an example of a basic SMT file:
```json
{
  "title": "My Song",
  "composer": "John Smith",
  "arranger": "Jane Doe",
  "tempo": {
    "bpm": 120
  },
  "key": {
    "root": "C",
    "mode": "major"
  },
  "time": "4/4",
  "parts": {
    "piano": [
      ["C4q", "D4h", "Rq", "F4q"],
      ["Rq", "A4q", "Rq", "C5h"]
    ],
    "bass": [
      ["Rh"],
      ["D3h", "Rq", "Rq", "G3w"],
      ["Rq", "C4q", "Rq", "E4q", "F4q"]
    ]
  }
}
```

This file defines a simple song with a title, composer, tempo, key, time signature, and one part for a piano. The `parts` section defines the notes in each part, with each note represented by a pitch and duration code.

SMT also supports the use of pauses or rests, which can be represented by the special note value of "r" with the appropriate duration code.

## Benefits

SMT is a simple and flexible format for representing music notation that is both easy to read and easy to parse. Its support for nested structures and the ability to represent notes as a series of beats make it ideal for representing complex rhythms and musical structures. SMT's simple syntax also makes it easy to work with manually or programmatically, and its support for JSON makes it easy to parse and manipulate in a variety of programming languages.

## Examples

### SMT Version 2.5
```json
{
  "title": "My Song",
  "composer": "John Smith",
  "arranger": "Jane Doe",
  "tempo": {
    "bpm": 120
  },
  "key": {
    "root": "C",
    "mode": "major"
  },
  "time": "4/4",
  "parts": {
    "piano": [
      ["C4q", "D4h", "Rq", "F4q"],
      ["Rq", "A4q", "Rq", "C5h"]
    ],
    "bass": [
      ["Rh"],
      ["D3h", "Rq", "Rq", "G3w"],
      ["Rq", "C4q", "Rq", "E4q", "F4q"]
    ]
  }
}
```

### SMT Version 1
```
; My Song
; John Smith
; C major, 4/4

piano
C4 4
D4 4
E4 4
F4 4
```

## History of development

| Version | Trigger | Description |
|---------|---------|-------------|
| 1.0     | Basic format | Initial version of SMT. Basic format for music notation with support for titles, composers, time signatures, keys, and individual notes. |
| 2.0     | Nested JSON | Update to SMT format. Nested JSON structure to better represent complex musical structures. New fields for arrangers and tempo. |
| 2.1     | Simplification | Simplification of SMT format. Time signatures now represented as simple fractions. Parts (formerly staffs) now represented as arrays of notes. |
| 2.2     | Rhythm and Rests | Enhanced SMT format. Parts now represented as series of beats rather than arrays of notes. Addition of support for pauses and rests. |
| 2.5 | Fixed pauses | Enhanced SMT format. Parts now represented as series of beats rather than arrays of notes. Addition of support for pauses and rests. Update to SMT format. Music pauses now represented with "Rq" and "Rh" to match note duration codes. |

## Brief recall of conversatio with ChatGPT

- We started by defining the Simple Music Text (SMT) format, a plain text format for representing music notation.
- We designed SMT version 1 with support for titles, composers, time signatures, keys, and individual notes.
- We then improved SMT to version 2 by adding a nested JSON structure and new fields for arrangers and tempo.
- We further simplified SMT in version 2.1 by simplifying the time signatures and the representation of parts.
- We enhanced SMT in version 2.2 by representing parts as series of beats and adding support for pauses and rests.
- We updated the SMT format to version 2.3, with music pauses now represented with uppercase "R" instead of lowercase "r".
- We updated the SMT format to version 2.4, with music pauses represented with "Rq" and "Rh" to match note duration codes. _(ChatGPT forgot what we actually did here)_
- We updated the SMT format to version 2.5, with music pauses represented with "Rq" and "Rh" and duration included.
- We provided an example of the latest SMT version 2.5, including all possible note variations and rests.
- Finally, we created a README.md file for SMT and provided a historical summary of its design in a markdown table.
